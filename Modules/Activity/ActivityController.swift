//
//  ActivityController.swift
//  Relax
//
//  Created by Honglin Yi on 9/30/18.
//  Copyright © 2018 henryyi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ActivityController: UIViewController {
    
    //Layout Parameters
    let lengthRatio: CGFloat = UIScreen.main.bounds.height >= 812.0 ? 0.7 : 0.6//0.5253
    lazy var topY: CGFloat = {
        let safeTop: CGFloat = 30.0//44.0
        //view.safeAreaLayoutGuide.layoutFrame.size.height
        return UIScreen.main.bounds.height >= 812.0 ? safeTop : 0.6
    }()
    
    //UI
    lazy var adView: GADBannerView = {
        let view = GADBannerView(frame: CGRect(x: 0, y: topY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * lengthRatio))
        view.backgroundColor = UIColor.hyCyan
        //view.adSize = kGADAdSizeFullBanner // 300 * 250
        let size = GADAdSizeFromCGSize(CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * lengthRatio))
        view.adSize = size
        return view
    }()
    
    lazy var appView: UIView = {
        let controller = AppsController(collectionViewLayout: UICollectionViewFlowLayout())

        let y = UIScreen.main.bounds.width * lengthRatio + topY
        let height = UIScreen.main.bounds.height - y
        let width = UIScreen.main.bounds.width

        controller.view.frame = CGRect(x: 0, y: y, width: width, height: height)
        self.addChildViewController(controller)
        
        return controller.view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.view.addSubview(adView)
        self.view.addSubview(appView)
        
        adView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adView.rootViewController = self
        let request = GADRequest()
        adView.load(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}
