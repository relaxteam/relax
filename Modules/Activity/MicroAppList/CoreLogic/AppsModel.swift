//
//  AppsModel.swift
//  RelaxApp2
//
//  Created by Honglin Yi on 2/27/18.
//  Copyright © 2018 Honglin Yi. All rights reserved.
//

import Foundation

struct AppsModel: Codable {
    let imageUrl: String
    let name: String
    let controllerName: String
    let htmlUrl: String
}

struct AppsContainer: Codable {
    var appsModels: [AppsModel]
}

