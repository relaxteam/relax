//
//  UIImageView+Downloader.swift
//  CoffeeSafe
//
//  Created by Honglin Yi on 9/16/18.
//  Copyright © 2018 LiveSafe, Inc. All rights reserved.
//

import Foundation
import UIKit
import Disk

extension UIImageView {
    
    func hy_setImage(with url:String, placeholderImage: UIImage? = nil,
                     complete:((Bool)->Void)? = nil) {
        
        guard let link = URL(string: url) else {
            setPlaceHolder(placeholderImage)
            complete?(false)
            return
        }
        
//        if let image = ImageCache.shared.get(with: url.absoluteString) {
//            self.image = image
//            complete?(true)
//            return
//        }
        ImageCache.shared.get(with: url) {
            guard let image = $0 else {
                self.setPlaceHolder(placeholderImage)
                self.download(url: url, link: link, complete: complete)
                return
            }
            DispatchQueue.main.async() {
                self.image = image
            }
        }

    }
    
    private func setPlaceHolder(_ placeholderImage: UIImage?) {
        if let placeholderImage = placeholderImage {
            DispatchQueue.main.async() {
                self.image = placeholderImage
            }
        }
    }
    
    private func download(url: String, link: URL, complete: ((Bool)->Void)?) {
        URLSession.shared.dataTask(with: link) { data, response, error in
            guard let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    complete?(false)
                    return
            }
            ImageCache.shared.add(key: url, image: image)
            DispatchQueue.main.async() {
                self.image = image
                complete?(true)
            }
        }.resume()
    }
    
}

class ImageCache {
    static let shared = ImageCache()
    private let directory = "Activity/"
    
    func add(key:String, image: UIImage) {
        guard let name = key.components(separatedBy: "/").last else {return}
        let path = directory + name
        try? Disk.save(image, to: .documents, as: path)
    }
    
    func get(with key:String, complete: @escaping (UIImage?)->Void ){
        guard let name = key.components(separatedBy: "/").last else {return}
        let path = directory + name
        guard let image = try? Disk.retrieve(path, from: .documents, as: UIImage.self)
        else {
            complete(nil)
            return
        }
        complete(image)
    }
    
}



