//
//  AppsController.swift
//  RelaxApp2
//
//  Created by Honglin Yi on 2/27/18.
//  Copyright © 2018 Honglin Yi. All rights reserved.
//

import UIKit


class AppsController: UICollectionViewController, UICollectionViewDelegateFlowLayout,AppsManagerDelegate{
    
    //MARK: - AppsManager
    func AppsManagerUpdate(_ models: [AppsModel]) {
        self.appss = models
        self.collectionView?.reloadData()
    }
    

    //MARK: - Internal
    private var appss = [AppsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.isScrollEnabled = true
        setupConllectionView()
        
        let manager = AppsManager()
        manager.delegate = self
    }
    
    //MARK: Controller Setup
    private lazy var layout:UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        let width = (UIScreen.main.bounds.width - 3.0)/3
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 1
        return layout
    }()
    func setupConllectionView() {
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = UIColor.white
        
        let nib2 = UINib(nibName: "AppsCell", bundle: nil)
        collectionView?.register(nib2, forCellWithReuseIdentifier: "Apps")
        
        collectionView?.collectionViewLayout = layout
    }
    
    
    //MARK: - Collection Delegate
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appss.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Apps", for: indexPath) as! AppsCell
        let apps = appss[indexPath.item]
        cell.addDatas(apps.imageUrl, apps.name)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? AppsCell
        //present.handleCellClick(indexPath.row, self, cell?.imageView.image)
        let row = indexPath.row
        let image = cell?.imageView.image
        if(appss[row].controllerName == "Web") {
            self.present(HYMicroController(aUrl: appss[row].htmlUrl, image: image, name: appss[row].name), animated: true, completion: nil)
        }
    }

}




fileprivate extension AppsController {
    
    
    func getMockup() {
        var paras = [String:Any]()
        paras["htmlName"] = "relax"
        let apps1 = AppsModel(imageUrl: "", name: "Relax", controllerName: "Web", htmlUrl: "https://www.baidu.com")
        
        let apps2 =  AppsModel(imageUrl: "", name: "Record", controllerName: "Web", htmlUrl: "https://medium.com/s/story/stop-banking-on-a-breakthrough-microshifts-are-what-will-change-your-life-54add1467545")
        
        let apps3 = AppsModel(imageUrl: "", name: "Bike", controllerName: "Web", htmlUrl: "https://stackoverflow.com/questions/52583040/regex-with-any-number-with-space-or-dash")
        
        let apps4 = AppsModel(imageUrl: "", name: "Music", controllerName: "Web", htmlUrl: "https://mp.weixin.qq.com/s/4hXBw7sZ-NKs_asOQxS7gA")
        
        let apps5 = AppsModel(imageUrl: "", name: "Music", controllerName: "Web", htmlUrl: "https://mp.weixin.qq.com/s/8JVOwEw-UMEB4bRuBHRoSw")
        
        appss.append(apps1)
        appss.append(apps2)
        appss.append(apps3)
        appss.append(apps4)
        appss.append(apps5)
    }
}
