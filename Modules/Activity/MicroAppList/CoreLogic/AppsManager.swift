//
//  AppsProvider.swift
//  Relax
//
//  Created by Honglin Yi on 10/8/18.
//  Copyright © 2018 yihl. All rights reserved.
//

import Foundation
import Disk


protocol AppsManagerDelegate {
    func AppsManagerUpdate(_ models: [AppsModel])
}


class AppsManager {
    let filePath = "Activity/Activity.json"
    
    var delegate: AppsManagerDelegate? {
        didSet {
            loadModels()
        }
    }
    
    func loadModels() {
        guard !LaunchChecker.isDailyFirst()  else {
            loadOnlineModels()
            return
        }
        loadOfflineModels() {
            loadOnlineModels()
        }
    }
    
    func updateModelsFromRemote() {
        loadOnlineModels()
    }
 
    private func loadOnlineModels() {
        let url = "https://s3.us-east-2.amazonaws.com/relaxapp/Activity/Activity.json"
        guard let link = URL.init(string: url) else { return }
        let task = URLSession.shared.dataTask(with: link) { data, response, error in
            guard let data = data,
                  let appsContainer = try? JSONDecoder().decode(AppsContainer.self, from: data) else { return }

            try? Disk.save(appsContainer, to: .documents, as: self.filePath)
            DispatchQueue.main.async() {
                self.delegate?.AppsManagerUpdate(appsContainer.appsModels)
            }
            
        }
        task.resume()
    }
    
    private func loadOfflineModels(complete: ()->Void) {
        guard let container = try? Disk.retrieve(filePath, from: .documents,
                                                 as: AppsContainer.self)
        else {
            let models = modelsFromBundle()
            self.delegate?.AppsManagerUpdate(models)
            complete()
            return
        }
        
        self.delegate?.AppsManagerUpdate(container.appsModels)
    }
    
    private func modelsFromBundle() -> [AppsModel] {
        let nilReturn = [AppsModel]()
        guard let path = Bundle.main.path(forResource: "Activity", ofType: "json")
            else { return nilReturn}
        let url = URL(fileURLWithPath: path)
        guard let data = try? Data.init(contentsOf: url, options: .mappedIfSafe)
            else { return nilReturn}
        guard let container = try? JSONDecoder().decode(AppsContainer.self, from: data)
            else { return nilReturn}
        return container.appsModels
    }
    
}
