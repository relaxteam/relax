//
//  LaunchCheck.swift
//  Relax
//
//  Created by Honglin Yi on 10/8/18.
//  Copyright © 2018 yihl. All rights reserved.
//

import Foundation

public class LaunchChecker {
    
    private static let FIRSTLAUNCH = "FIRSTLAUNCH"
    
    public static func isDailyFirst() -> Bool {
    let today = Date().formatted
    guard let date = UserDefaults.standard.string(forKey: FIRSTLAUNCH) else {
        UserDefaults.standard.set(today, forKey: FIRSTLAUNCH)
        return true
    }
    guard date == today else {
        UserDefaults.standard.set(today, forKey: FIRSTLAUNCH)
        return true
    }
    return false
    }
}

fileprivate extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return  formatter.string(from: self)
    }
}
